module.exports = shipit => {
    // Load shipit-deploy tasks
    require('shipit-deploy')(shipit)
  
    const appName = 'qaschool';


    shipit.initConfig({
      default: {
        //copy: false,
        deployTo: 'qaschool',
        repositoryUrl: 'https://IgorKorshuk@bitbucket.org/IgorKorshuk/instavue.git',
        shared: {
            overwrite: true,
            dirs: ['uploads']
        }
      },
      production: {
        servers: 'ubuntu@34.248.247.83',
        key: 'key.pem',
      },
    })
    shipit.on('deploy', () => {
        shipit.start('say-hello');
    });

    shipit.on('updated', () => {
        shipit.start('npm-install');
    });
    
    shipit.on('published', () => {
        shipit.start('pm2-server');
    });
      
    shipit.blTask('say-hello', async () => {
        shipit.local('echo "hello from your local computer"')
    });

    shipit.blTask('npm-install', async () => {
        shipit.remote(`cd ${shipit.releasePath} && npm install`);
    });

    shipit.blTask('pm2-server', async () => {
        await shipit.remote(`pm2 delete -s ${appName} || :`);
        await shipit.remote(`pm2 start ${shipit.releasePath}/server.js --name ${appName}`);
    });
}