const companies = require("../models/companies");

function errorHandler(err, res) {
    res.status(500).send(err);
    throw (err);
}

const companiesData = {
  addNewCompany(req, res) {
    const company = req.body;
    companies.create( { name: company.name } )
    .then((data) => {
        if (data) {
            res.status(200).send({ type: "success", text: "ok" });
        }
    })
    .catch((err) => errorHandler(err, res));
  },

  getAllCompany(req, res) {
    companies
        .find({})
        .then((data) => {
            res.send(data);
        })
        .catch((err) => errorHandler(err, res));
  },

  updateName(req, res) {
    const company = req.body;
    companies.updateOne(
                        { _id: company.current },
                        { name: company.name }
                      ).then((data) => {
                          if (data) {
                            res.status(200).send({ type: "success", text: "update" });
                          }
                      })
                      .catch((err) => errorHandler(err, res));
  }
}
module.exports = companiesData;
