const mongoose = require("mongoose");

const companiesSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  }
});

module.exports = mongoose.model("companies", companiesSchema);
