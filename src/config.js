export const serverURL = "";

export const messagesText = {
    incorrectConfirmMessage: { text: "Incorrect password on the second field!", type: "error" },
    successEditing: { text: "Your data is saved successfully!", type: "error" }
};